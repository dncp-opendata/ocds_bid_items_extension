# Bid Items Extension

When a tenderer submits a bid for an item this bid may include, besides the total amount of the bid, also the product specification of the item as the brand, model, etc. And it also may include the unit.name and unit.value of it.

This extension adds the items array into the bid object into broken down the bid. For the product specification the local [items attributes extension](https://gitlab.com/dncp-opendata/ocds_item_attributes_extension) is recommended to be used.

## Changelog

This extension was originally discussed in <https://github.com/open-contracting/ocds-extensions/issues/126>.
